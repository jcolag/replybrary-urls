# Replybrary-URLs

URLs for Replybrary

In late November, I launched [@replybrary](https://twitter.com/replybrary), which is a Twitter-bot that uses code from [**library-twitterbot**](https://github.com/jcolag/library-twtterbot) to function.  The JSON file is, in effect, the database that backs the bot.

If the collection of URLs is useful to you, that's fine, but I have mostly made it public in order to solicit additions.  I have some standards to follow in [`CONTRIBUTING.md`](./CONTRIBUTING.md), but please feel free to add items in a pull request or file an issue with recommendations.
