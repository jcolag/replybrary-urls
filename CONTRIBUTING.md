All are welcome to help out, but please ensure that sources are:

 * **Real**, as in not likely to become dead links any time soon.
 * **Legitimate**, as in research or journalistic analysis of research, rather than a blog post.
 * **Not Encyclopedic**, because people can find their own links to Wikipedia or whatnot.
 * **Backed by Research**, as in, not an opinion piece that fails to refer to anything else.  If the research was funded by a think tank founded to prove what the research says is not actually research.
 * **General**, so that the contents are likely to apply more than a piece that only focuses on certain kinds of American households.

In other words, I won't accept any stories that are right-wing propaganda.  Or ads.

Also, be respectful of others.  If you act like a child throwing a tantrum, you're going to be treated as one.